﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CrossPoster.Server.Attributes;

namespace CrossPoster.Server.Controllers
{
    [RoutePrefix("api/test")]
    public class TestController : ApiController
    {

        [Route("")]
        public string Get()
        {
            return "You Get the String";
        }
    }
}
