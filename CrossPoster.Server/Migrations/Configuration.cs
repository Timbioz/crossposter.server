using CrossPoster.Server.Infrastructure;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CrossPoster.Server.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration :
        DbMigrationsConfiguration<CrossPoster.Server.Infrastructure.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(CrossPoster.Server.Infrastructure.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.
            var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));

            var user = new ApplicationUser()
            {
                UserName = "Timbioz",
                Email = "volkovtim@gmail.com",
                EmailConfirmed = true,
                FirstName = "Artem",
                LastName = "Volkov",
                Level = 1,
                JoinDate = DateTime.Now.AddYears(-3)
            };

            manager.Create(user, "!Aqwa1102Psv85");

            if (roleManager.Roles.Count() == 0)
            {
                roleManager.Create(new IdentityRole {Name = "SuperAdmin"});
                roleManager.Create(new IdentityRole {Name = "Admin"});
                roleManager.Create(new IdentityRole {Name = "User"});
            }

            var adminUser = manager.FindByName("Timbioz");

            manager.AddToRoles(adminUser.Id, new string[] {"SuperAdmin", "Admin"});
        }
    }
}